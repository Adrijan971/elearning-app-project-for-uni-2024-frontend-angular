import { Routes } from '@angular/router';
import { AuthGuard } from './AuthGuard';

import { StudentCreateComponent } from './students/student-create/student-create.component';
import { StudentListComponent } from './students/student-list/student-list.component';
import { StudentEditComponent } from './students/student-edit/student-edit.component';
import { StudentDetailsComponent } from './students/student-details/student-details.component';

import { AdministratorEditComponent } from './administrators/administrator-edit/administrator-edit.component';
import { AdministratorListComponent } from './administrators/administrator-list/administrator-list.component';
import { AdministratorDetailsComponent } from './administrators/administrator-details/administrator-details.component';
import { AdministratorCreateComponent } from './administrators/administrator-create/administrator-create.component';

import { TeacherCreateComponent } from './teachers/teacher-create/teacher-create.component';
import { TeacherDetailsComponent } from './teachers/teacher-details/teacher-details.component';
import { TeacherEditComponent } from './teachers/teacher-edit/teacher-edit.component';
import { TeacherListComponent } from './teachers/teacher-list/teacher-list.component';

import { SubjectCreateComponent } from './subjects/subject-create/subject-create.component';
import { SubjectEditComponent } from './subjects/subject-edit/subject-edit.component';
import { SubjectDetailsComponent } from './subjects/subject-details/subject-details.component';
import { SubjectListComponent } from './subjects/subject-list/subject-list.component';
import { SubjectListForStudentComponent } from './subjects/subject-list-for-student/subject-list-for-student.component';
import { SubjectListForTeacherComponent } from './subjects/subject-list-for-teacher/subject-list-for-teacher.component';

import { ExamCreateComponent } from './exams/exam-create/exam-create.component';
import { ExamDetailsComponent } from './exams/exam-details/exam-details.component';
import { ExamEditComponent } from './exams/exam-edit/exam-edit.component';
import { ExamListComponent } from './exams/exam-list/exam-list.component';

import { PaymentCreateComponent } from './payments/payment-create/payment-create.component';
import { PaymentEditComponent } from './payments/payment-edit/payment-edit.component';
import { PaymentListComponent } from './payments/payment-list/payment-list.component';

import { MainLoginFormComponent } from './loginForms/main-login-form/main-login-form.component';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';

import { ExamApplicationComponent } from './exam-applications/exam-application-create/exam-application.component';
import { ExamApplicationListComponent } from './exam-applications/exam-application-list/exam-application-list.component';
import { ExamListForTeacherComponent } from './exams/exam-list-for-teacher/exam-list-for-teacher.component';
import { MyPaymentListComponent } from './payments/my-payment-list/my-payment-list.component';
import { ExamApplicationsStudentComponent } from './exam-applications/exam-applications-student/exam-applications-student.component';
import { ExamApplicationsTeacherComponent } from './exam-applications/exam-applications-teacher/exam-applications-teacher.component';
import { AssignStudentSubjectComponent } from './assign/assign-student-subject/assign-student-subject.component';
import { AssignTeacherSubjectComponent } from './assign/assign-teacher-subject/assign-teacher-subject.component';
import { DocumentUploadComponent } from './documents/document-create/document-upload.component';
import { DocumentListComponent } from './documents/documents-list/document-list.component';
import { DocumentListForStudentComponent } from './documents/document-list-for-student/document-list-for-student.component';
import { Pdf } from './pdf/pdf.component';



export const routes: Routes = [
    { path: 'student/create', component: StudentCreateComponent, canActivate: [AuthGuard], data: { roles: ['ADMINISTRATOR']} },
    { path: 'student/list', component: StudentListComponent, canActivate: [AuthGuard], data: { roles: ['ADMINISTRATOR', 'TEACHER']} },
    { path: 'student/edit/:id', component: StudentEditComponent, canActivate: [AuthGuard], data: { roles: ['ADMINISTRATOR']} },
    { path: 'student/details:id', component: StudentDetailsComponent, canActivate: [AuthGuard] },

    { path: 'administrator/create', component: AdministratorCreateComponent, canActivate: [AuthGuard], data: { roles: ['ADMINISTRATOR']} },
    { path: 'administrator/details/:id', component: AdministratorDetailsComponent, canActivate: [AuthGuard] },
    { path: 'administrator/edit/:id', component: AdministratorEditComponent, canActivate: [AuthGuard], data: { roles: ['ADMINISTRATOR']} },
    { path: 'administrator/list', component: AdministratorListComponent, canActivate: [AuthGuard], data: { roles: ['ADMINISTRATOR', 'TEACHER']} },

    { path: 'teacher/create', component: TeacherCreateComponent, canActivate: [AuthGuard], data: { roles: ['ADMINISTRATOR']} },
    { path: 'teacher/details/:id', component: TeacherDetailsComponent, canActivate: [AuthGuard] },
    { path: 'teacher/edit/:id', component: TeacherEditComponent, canActivate: [AuthGuard], data: { roles: ['ADMINISTRATOR']} },
    { path: 'teacher/list', component: TeacherListComponent, canActivate: [AuthGuard], data: { roles: ['ADMINISTRATOR', 'TEACHER', "STUDENT"]} },

    { path: 'subject/create', component: SubjectCreateComponent, canActivate: [AuthGuard], data: { roles: ['ADMINISTRATOR']} },
    { path: 'subject/details/:id', component: SubjectDetailsComponent, canActivate: [AuthGuard] },
    { path: 'subject/edit/:id', component: SubjectEditComponent, canActivate: [AuthGuard], data: { roles: ['ADMINISTRATOR']} },
    { path: 'subject/list', component: SubjectListComponent, canActivate: [AuthGuard], data: { roles: ['ADMINISTRATOR', 'TEACHER', "STUDENT"]} },
    { path: 'subjectForStudent/list', component: SubjectListForStudentComponent, canActivate: [AuthGuard], data: { roles: ["STUDENT"]} },
    { path: 'subjectForTeacher/list', component: SubjectListForTeacherComponent, canActivate: [AuthGuard], data: { roles: ["TEACHER"]} },

    { path: 'exam/create', component: ExamCreateComponent, canActivate: [AuthGuard], data: { roles: ['ADMINISTRATOR', 'TEACHER']} },
    { path: 'exam/details/:id', component: ExamDetailsComponent, canActivate: [AuthGuard] },
    { path: 'exam/edit/:id', component: ExamEditComponent, canActivate: [AuthGuard], data: { roles: ['ADMINISTRATOR', 'TEACHER']} },
    { path: 'exam/list', component: ExamListComponent, canActivate: [AuthGuard], data: { roles: ['ADMINISTRATOR', 'TEACHER', "STUDENT"]} },
    { path: 'examForTeacher/list', component: ExamListForTeacherComponent, canActivate: [AuthGuard], data: { roles: ['TEACHER']} },

    { path: 'payment/create', component: PaymentCreateComponent, canActivate: [AuthGuard], data: { roles: ['ADMINISTRATOR']} },
    { path: 'payment/edit/:id', component: PaymentEditComponent, canActivate: [AuthGuard], data: { roles: ['ADMINISTRATOR']} },
    { path: 'payment/list', component: PaymentListComponent, canActivate: [AuthGuard], data: { roles: ['ADMINISTRATOR', 'TEACHER']} },
    { path: 'myPayment/list', component: MyPaymentListComponent, canActivate: [AuthGuard], data: { roles: ['STUDENT']} },

    { path: 'exam-application/create', component: ExamApplicationComponent, canActivate: [AuthGuard], data: { roles: [ "STUDENT"]} },
    { path: 'exam-application/list', component: ExamApplicationListComponent, canActivate: [AuthGuard], data: { roles: ['ADMINISTRATOR', 'TEACHER', "STUDENT"]} },
    { path: 'exam-applications-student/list', component: ExamApplicationsStudentComponent, canActivate: [AuthGuard], data: { roles: ["STUDENT"]} },
    { path: 'exam-applications-teacher/list', component: ExamApplicationsTeacherComponent, canActivate: [AuthGuard], data: { roles: ["TEACHER"]} },

    { path: 'assign/teacher-subject', component: AssignTeacherSubjectComponent, canActivate: [AuthGuard], data: { roles: ['ADMINISTRATOR']} },
    { path: 'assign/student-subject', component: AssignStudentSubjectComponent, canActivate: [AuthGuard], data: { roles: ['ADMINISTRATOR']} },

    { path: 'documents/upload', component: DocumentUploadComponent, canActivate: [AuthGuard], data: { roles: [ "STUDENT"]} },
    { path: 'documents/list', component: DocumentListComponent, canActivate: [AuthGuard], data: { roles: [ "ADMINISTRATOR"]} },
    { path: 'myDocuments/list', component: DocumentListForStudentComponent, canActivate: [AuthGuard], data: { roles: [ "STUDENT"]} },

    { path: 'login', component: MainLoginFormComponent },
    { path: 'unauthorized', component: UnauthorizedComponent },

    { path: 'pdf/:id', component: Pdf },

  ];

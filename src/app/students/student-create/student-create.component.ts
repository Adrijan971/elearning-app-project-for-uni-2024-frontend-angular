import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
// import { MatErrorModule } from '@angular/material/form-field';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { StudentService } from '../../services/student.service';
import { StudentCreateDTO } from '../../DTOs/createDTOs/studentCreateDTO.model';
import { Router, RouterModule } from '@angular/router';
import { MatSnackBarModule, MatSnackBar } from '@angular/material/snack-bar';



@Component({
  selector: 'app-student-create',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatDatepickerModule,
    MatToolbarModule,
    MatNativeDateModule,
    MatIconModule
  ],
  templateUrl: './student-create.component.html',
  styleUrl: './student-create.component.css'
})
export class StudentCreateComponent {

  studentForm: FormGroup;
  submitted = false;

  constructor(private fb: FormBuilder, private studentService: StudentService, private router: Router,  private snackBar: MatSnackBar ) {
    this.studentForm = this.fb.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      username: ['', [Validators.required, Validators.pattern(/@/)]],
      password: ['', Validators.required],
    });
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.studentForm.valid) {
      const studentCreatetDTO: StudentCreateDTO = {
        firstName: this.studentForm.get('firstname')?.value,
        lastName: this.studentForm.get('lastname')?.value,
        username: this.studentForm.get('username')?.value,
        password: this.studentForm.get('password')?.value,
        userType: "STUDENT",
        // indexNumber:1   // hardcoded for now, change to generate on backend
      }; 

      this.studentService.createStudent(studentCreatetDTO).subscribe(
        (response) => {
          console.log('Student created successfully:', response);

        this.snackBar.open('Student created successfully!', 'Close', {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
        });

        this.router.navigate([`/student/list`]);
        },
        (error) => {
          console.error('Error creating Student:', error);

          this.snackBar.open('Error, Student was not created!', 'Close', {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
        });
        }
      );
    }
  }


}

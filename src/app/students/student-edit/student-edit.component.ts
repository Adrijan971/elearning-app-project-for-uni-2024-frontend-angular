import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { StudentService } from '../../services/student.service'; // Import the service
import { Student } from '../../models/student.model';
import { StudentUpdateDTO } from '../../DTOs/updateDTOs/studentUpdateDTO.model';
import { MatSnackBarModule, MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-student-edit',
  standalone: true,
  imports: [
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatDividerModule,
    CommonModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatIconModule
  ],
  templateUrl: './student-edit.component.html',
  styleUrl: './student-edit.component.css'
})
export class StudentEditComponent implements OnInit {

  studentForm!: FormGroup;
  studentId!: number;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private studentService: StudentService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    // Initialize the form
    this.studentForm = this.fb.group({
      username: ['', [Validators.required, Validators.email]],
      name: ['', Validators.required],
      lastname: ['', Validators.required],
      address: ['', Validators.required],
      mobilePhone: ['', Validators.required],
    });

    // Get student ID from the route
    this.studentId = +this.route.snapshot.paramMap.get('id')!;
    this.loadStudentData(this.studentId);
  }

  // Use the StudentService to load student data
  loadStudentData(id: number): void {
    this.studentService.getStudent(id).subscribe(
      (student: Student) => {
        this.studentForm.patchValue(student);
      },
      (error) => {
        console.error('Failed to load student', error);
      }
    );
  }

  // Handle form submission and use the updateStudent method from the service
  onSubmit(): void {
    if (this.studentForm.valid) {
      const updatedStudent: StudentUpdateDTO = this.studentForm.value;
      this.studentService.updateStudent(this.studentId, updatedStudent).subscribe(
        (response) => {
          console.log('Student updated successfully:', response);

          this.snackBar.open('Student updated successfully!', 'Close', {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
          });
          this.router.navigate(['student/list']); // Navigate to the student list after saving
        },
        (error) => {
          console.error('Error creating Student:', error);

          this.snackBar.open('Error, Student was not updated!', 'Close', {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
        })}
      );
    }
  }

  onCancel(): void {
    this.router.navigate(['student/list']); // Navigate back on cancel
  }

}

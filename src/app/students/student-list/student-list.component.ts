import { Component, OnInit } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { RouterModule, Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { StudentService } from '../../services/student.service';
import { Student } from '../../models/student.model';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-student-list',
  standalone: true,
  imports: [
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    RouterModule,
    CommonModule,
    MatToolbarModule
  ],
  templateUrl: './student-list.component.html',
  styleUrl: './student-list.component.css'
})
export class StudentListComponent implements OnInit {

  students: Student[] = [];
  displayedColumns: string[] = ['id', 'username', 'indexNumber', 'name','lastname', 'balance','archived', 'actions'];

  userRole: string = '';

  constructor(
    private studentService: StudentService, // Inject the service
    private router: Router,
    private authService : AuthService
  ) {
    this.userRole = this.authService.getCurrentUserRole() || "";
  }

  isAdministrator(): boolean {
    return this.userRole === 'ADMINISTRATOR';
  }

  isTeacher(): boolean {
    return this.userRole === 'TEACHER';
  }

  isStudent(): boolean {
    return this.userRole === 'STUDENT';
  }

  ngOnInit(): void {

    if(!this.isAdministrator()){
      this.displayedColumns = ['id', 'username', 'indexNumber', 'name','lastname','archived', 'actions'];
    }

    // Fetch students when the component initializes
    this.loadStudents();
  }

  // Method to load students using the StudentService
  loadStudents(): void {
    this.studentService.getStudents().subscribe(
      (data: Student[]) => {
        this.students = data;
      },
      (error) => {
        console.error('Failed to load students', error);
      }
    );
  }

  editStudent(id: number): void {
    this.router.navigate([`/student/edit/${id}`]); // Navigate to the edit page
  }

  deleteStudent(id: number): void {
    this.studentService.deleteStudent(id).subscribe(
      () => {
        console.log('Deleted student with ID:', id);
        this.loadStudents(); // Reload students after deletion
      },
      (error) => {
        console.error('Failed to delete student', error);
      }
    );
  }

}

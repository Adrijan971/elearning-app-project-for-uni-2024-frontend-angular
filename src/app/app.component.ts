import { Component, OnInit, ViewChild  } from '@angular/core';
import { Router, RouterModule, RouterOutlet } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { MatSidenavModule, MatDrawer  } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { StudentLoginFormComponent } from './loginForms/student-login-form/student-login-form.component';
import { MainLoginFormComponent } from './loginForms/main-login-form/main-login-form.component';
import { SidebarComponent } from './sidebar/sidebar.component';

import { CommonModule } from '@angular/common';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    RouterOutlet,
    RouterModule,
    MainLoginFormComponent,
    HeaderComponent,
    SidebarComponent,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    StudentLoginFormComponent,
    CommonModule,
    SidebarComponent
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent implements OnInit{

  // by default it is login component
  componentToShow: string = 'login';

  sidebarActionCount: number = 0;

  constructor(
    private authService: AuthService,
    private router: Router
  ){}

  ngOnInit(): void {
    if (!this.authService.isAuthenticated() && this.router.url !== '/login') {
      // Redirect to another route if authenticated
      this.router.navigate(['/login']);
    }
  }

  handleBurgerButtonClick() {
    // Increment the counter with each click so that 'change' is sent every time.
    this.sidebarActionCount++;
  }

  showComponent(componentToShow: string): void {
    this.componentToShow = componentToShow;
  }
}

import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
// import { MatErrorModule } from '@angular/material/form-field';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { AdministratorService } from '../../services/administrator.service';
import { AdministratorCreateDTO } from '../../DTOs/createDTOs/administratorCreateDTO.model';
import { MatSnackBarModule, MatSnackBar } from '@angular/material/snack-bar';
import { Router, RouterModule } from '@angular/router';


@Component({
  selector: 'app-administrator-create',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatDatepickerModule,
    MatToolbarModule,
    MatNativeDateModule,
    MatIconModule
  ],
  templateUrl: './administrator-create.component.html',
  styleUrl: './administrator-create.component.css'
})
export class AdministratorCreateComponent {

  administratorForm: FormGroup;
  submitted = false;

  constructor(private fb: FormBuilder, private administratorService: AdministratorService, private router: Router,  private snackBar: MatSnackBar ) {
    this.administratorForm = this.fb.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      username: ['', [Validators.required, Validators.pattern(/@/)]],
      password: ['', Validators.required],
    });
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.administratorForm.valid) {
      const administratorCreatetDTO: AdministratorCreateDTO = {
        firstName: this.administratorForm.get('firstname')?.value,
        lastName: this.administratorForm.get('lastname')?.value,
        username: this.administratorForm.get('username')?.value,
        password: this.administratorForm.get('password')?.value,
        userType: "ADMINISTRATOR",
      }; 

      this.administratorService.createStudent(administratorCreatetDTO).subscribe(
        (response) => {
          console.log('Administrator created successfully:', response);

        this.snackBar.open('Administrator created successfully!', 'Close', {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
        });

        this.router.navigate([`/administrator/list`]);
        },
        (error) => {
          console.error('Error creating Administrator:', error);

          this.snackBar.open('Error, Administrator was not created!', 'Close', {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
        });
        }
      );
    }
  }

}

import { Component } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { RouterModule, Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { AdministratorService } from '../../services/administrator.service';

interface Administrator {
  id: number;
  name: string;
  email: string;
}

@Component({
  selector: 'app-administrator-list',
  standalone: true,
  imports: [
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    RouterModule,
    CommonModule,
    MatToolbarModule,
  ],
  templateUrl: './administrator-list.component.html',
  styleUrl: './administrator-list.component.css'
})
export class AdministratorListComponent {

  administrators: Administrator[] = [];
  displayedColumns: string[] = ['id', 'username', 'name','lastname', 'archived', 'actions'];

  constructor(
    private administratorService: AdministratorService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loadAdministrators();
  }

  loadAdministrators(): void {
    this.administratorService.getAdministrators().subscribe(
      (data: Administrator[]) => {
        this.administrators = data;
      },
      (error) => {
        console.error('Failed to load administrators', error);
      }
    );
  }

  editAdministrator(id: number): void {
    this.router.navigate([`/administrator/edit/${id}`]);
  }
  
  deleteAdministrator(id: number): void {
    this.administratorService.deleteAdministrator(id).subscribe(
      () => {
        console.log('Deleted administrator with ID:', id);
        this.loadAdministrators(); // Reload administrators after deletion
      },
      (error) => {
        console.error('Failed to delete administrator', error);
      }
    );
  }

}

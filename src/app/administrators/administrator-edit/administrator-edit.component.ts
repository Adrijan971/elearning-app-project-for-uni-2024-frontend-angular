import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { AdministratorService } from '../../services/administrator.service';
import { Administrator } from '../../models/administrator.model';
import { AdministratorUpdateDTO } from '../../DTOs/updateDTOs/administratorUpdateDTO.model';
import { MatSnackBarModule, MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-administrator-edit',
  standalone: true,
  imports: [
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatDividerModule,
    CommonModule,
    ReactiveFormsModule, // Import the ReactiveFormsModule for form handling
    MatToolbarModule,
    MatIconModule
  ],
  templateUrl: './administrator-edit.component.html',
  styleUrl: './administrator-edit.component.css'
})
export class AdministratorEditComponent implements OnInit{

  administratorForm!: FormGroup;
  administratorId!: number;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private administratorService: AdministratorService,
    private snackBar: MatSnackBar

  ) {}

  ngOnInit(): void {
    // Initialize the form
    this.administratorForm = this.fb.group({
      username: ['', [Validators.required, Validators.email]],
      name: ['', Validators.required],
      lastname: ['', Validators.required],
      address: ['', Validators.required],
      mobilePhone: ['', Validators.required],
    });

    this.administratorId = +this.route.snapshot.paramMap.get('id')!;
    // Fetch the administrator data and patch the form (replace with actual service call)
    this.loadAdministratorData(this.administratorId);
  }

  loadAdministratorData(id: number) {
    this.administratorService.getAdministrator(id).subscribe(
      (administrator: Administrator) => {
        this.administratorForm.patchValue(administrator);
      },
      (error) => {
        console.error('Failed to load administrator.', error);
      }
    );
  }

  onSubmit() {
    if (this.administratorForm.valid) {
      const updatedAdministrator: AdministratorUpdateDTO = this.administratorForm.value;
      this.administratorService.updateAdministrator(this.administratorId, updatedAdministrator).subscribe(
        (response) => {
          console.log('Administrator updated successfully:', response);

          this.snackBar.open('Administrator updated successfully!', 'Close', {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
          });
          this.router.navigate(['administrator/list']); // Navigate to the administrator list after saving
        },
        (error) => {
          console.error('Error creating Administrator:', error);

          this.snackBar.open('Error, Administrator was not updated!', 'Close', {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
        })}
      );
    }
  }

  onCancel() {
    this.router.navigate(['administrator/list']); // Navigate back on cancel
  }


}

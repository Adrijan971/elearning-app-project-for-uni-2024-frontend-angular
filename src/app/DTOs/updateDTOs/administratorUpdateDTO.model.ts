export interface AdministratorUpdateDTO {
  username : string;
  name: string;
  lastname : string;
  password : string;
  address : string;
  mobilePhone : number;
}
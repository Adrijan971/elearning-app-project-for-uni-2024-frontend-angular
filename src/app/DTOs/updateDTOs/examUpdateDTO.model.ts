export interface ExamUpdateDTO {
  teacherId : number;
  dateTime: string;
}
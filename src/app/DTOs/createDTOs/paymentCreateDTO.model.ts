export interface PaymentCreateDTO {
  bankAccount: string;
  amount: number;
  dateTime: string;
  studentId: number;
}
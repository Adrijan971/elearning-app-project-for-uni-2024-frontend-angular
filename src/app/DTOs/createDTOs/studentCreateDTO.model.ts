export interface StudentCreateDTO {
  username : string;
  firstName: string;
  lastName : string;
  password : string;
  userType : string;
}
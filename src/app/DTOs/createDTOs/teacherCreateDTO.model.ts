export interface TeacherCreateDTO {
  username : string;
  firstName: string;
  lastName : string;
  password : string;
  userType : string;
  teacherType : string;
}
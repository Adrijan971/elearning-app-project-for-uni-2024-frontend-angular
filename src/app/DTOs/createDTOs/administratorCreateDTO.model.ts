export interface AdministratorCreateDTO {
  username : string;
  firstName: string;
  lastName : string;
  password : string;
  userType : string;
}
export interface ApplicationCreateDTO {
  examId : number;
  studentId: number;
  applicationDate : string;
}
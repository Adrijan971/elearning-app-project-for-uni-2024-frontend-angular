export interface SubjectCreateDTO {
  name: string;
  ects: number;
}
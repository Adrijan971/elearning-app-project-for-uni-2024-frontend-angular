export interface ExamCreateDTO {
  name: string;
  teacherId: number;
  subjectId: number;
  dateTime: string;
}
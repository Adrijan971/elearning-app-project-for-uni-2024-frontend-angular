import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { SubjectService } from '../../services/subject.service';
import { Subject } from '../../models/subject.model';
import { SubjectUpdateDTO } from '../../DTOs/updateDTOs/subjectUpdateDTO.model';
import { MatSnackBarModule, MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-subject-edit',
  standalone: true,
  imports: [
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatDividerModule,
    CommonModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatIconModule
  ],
  templateUrl: './subject-edit.component.html',
  styleUrl: './subject-edit.component.css'
})
export class SubjectEditComponent implements OnInit{

  subjectForm!: FormGroup;
  subjectId!: number;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private subjectService: SubjectService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    // Initialize the form
    this.subjectForm = this.fb.group({
      name: ['', Validators.required],
      ects: ['', Validators.required],
    });

    // Get subject ID from the route
    this.subjectId = +this.route.snapshot.paramMap.get('id')!;
    this.loadSubjectData(this.subjectId);
  }

  // Use the SubjectService to load subject data
  loadSubjectData(id: number): void {
    this.subjectService.getSubject(id).subscribe(
      (subject: Subject) => {
        this.subjectForm.patchValue(subject);
      },
      (error) => {
        console.error('Failed to load subject', error);
      }
    );
  }

  // Handle form submission and use the updateSubject method from the service
  onSubmit(): void {
    if (this.subjectForm.valid) {
      const updatedSubject: SubjectUpdateDTO = this.subjectForm.value;
      this.subjectService.updateSubject(this.subjectId, updatedSubject).subscribe(
        (response) => {
          console.log('Subject updated successfully:', response);

          this.snackBar.open('Subject updated successfully!', 'Close', {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
          });
          this.router.navigate(['subject/list']); // Navigate to the subject list after saving
        },
        (error) => {
          console.error('Error creating Subject:', error);

          this.snackBar.open('Error, Subject was not updated!', 'Close', {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
        })}
      );
    }
  }

  onCancel(): void {
    this.router.navigate(['subject/list']);
  }

}

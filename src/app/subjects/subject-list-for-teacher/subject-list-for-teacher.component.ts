import { Component, OnInit } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { RouterModule, Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { SubjectService } from '../../services/subject.service';
import { Subject } from '../../models/subject.model';
import { TeacherService } from '../../services/teacher.service';

@Component({
  selector: 'app-subject-list',
  standalone: true,
  imports: [
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    RouterModule,
    CommonModule,
    MatToolbarModule
  ],
  templateUrl: './subject-list-for-teacher.component.html',
  styleUrl: './subject-list-for-teacher.component.css'
})
export class SubjectListForTeacherComponent implements OnInit{

  subjects: Subject[] = [];
  displayedColumns: string[] = ['name', 'ects'];

  constructor(
    private subjectService: SubjectService,
    private teacherService: TeacherService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loadSubjects();
  }

  loadSubjects(): void {
    this.teacherService.getSubjectsForTeacher(Number(localStorage.getItem("userId"))).subscribe(
      (data: Subject[]) => {
        this.subjects = data;
      },
      (error) => {
        console.error('Failed to load subjects', error);
      }
    );

  }

  editSubject(id: number): void {
    this.router.navigate([`/subject/edit/${id}`]); // Navigate to the edit page
  }

  deleteSubject(id: number): void {
    this.subjectService.deleteSubject(id).subscribe(
      () => {
        console.log('Deleted subject with ID:', id);
        this.loadSubjects(); // Reload subjects after deletion
      },
      (error) => {
        console.error('Failed to delete subject', error);
      }
    );
  }


}

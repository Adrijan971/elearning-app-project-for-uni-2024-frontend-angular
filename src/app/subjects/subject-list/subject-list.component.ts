import { Component, OnInit } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { RouterModule, Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { SubjectService } from '../../services/subject.service';
import { Subject } from '../../models/subject.model';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-subject-list',
  standalone: true,
  imports: [
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    RouterModule,
    CommonModule,
    MatToolbarModule
  ],
  templateUrl: './subject-list.component.html',
  styleUrl: './subject-list.component.css'
})
export class SubjectListComponent implements OnInit{

  subjects: Subject[] = [];
  displayedColumns: string[] = ['id', 'name', 'ects', 'archived', 'actions'];

  userRole: string = '';

  constructor(
    private subjectService: SubjectService, // Inject the service
    private router: Router,
    private authService : AuthService,
  ) {
    this.userRole = this.authService.getCurrentUserRole() || "";
  }

  isAdministrator(): boolean {
    return this.userRole === 'ADMINISTRATOR';
  }

  isTeacher(): boolean {
    return this.userRole === 'TEACHER';
  }

  isStudent(): boolean {
    return this.userRole === 'STUDENT';
  }

  ngOnInit(): void {
    this.loadSubjects();
  }

  loadSubjects(): void {
    this.subjectService.getSubjects().subscribe(
      (data: Subject[]) => {
        this.subjects = data;
      },
      (error) => {
        console.error('Failed to load subjects', error);
      }
    );

  }

  editSubject(id: number): void {
    this.router.navigate([`/subject/edit/${id}`]); // Navigate to the edit page
  }

  deleteSubject(id: number): void {
    this.subjectService.deleteSubject(id).subscribe(
      () => {
        console.log('Deleted subject with ID:', id);
        this.loadSubjects(); // Reload subjects after deletion
      },
      (error) => {
        console.error('Failed to delete subject', error);
      }
    );
  }


}

import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
// import { MatErrorModule } from '@angular/material/form-field';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { SubjectService } from '../../services/subject.service';
import { SubjectCreateDTO } from '../../DTOs/createDTOs/subjectCreateDTO.model';
import { Router, RouterModule } from '@angular/router';
import { MatSnackBarModule, MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-subject-create',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatDatepickerModule,
    MatToolbarModule,
    MatNativeDateModule,
    MatIconModule,
    RouterModule,
    MatSnackBarModule,
  ],
  templateUrl: './subject-create.component.html',
  styleUrl: './subject-create.component.css'
})
export class SubjectCreateComponent {

  subjectForm: FormGroup;
  submitted = false;

  constructor(private fb: FormBuilder, private subjectService: SubjectService, private router: Router,  private snackBar: MatSnackBar ) {
    this.subjectForm = this.fb.group({
      subjectName: ['', Validators.required],
      subjectEcts: ['', [Validators.required, Validators.min(1)]],

    });
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.subjectForm.valid) {
      const subjectDTO: SubjectCreateDTO = {
        name: this.subjectForm.get('subjectName')?.value,
        ects: this.subjectForm.get('subjectEcts')?.value,
      };

      // Call the service to create the subject
      this.subjectService.createSubject(subjectDTO).subscribe(
        (response) => {
          console.log('Subject created successfully:', response);

        this.snackBar.open('Subject created successfully!', 'Close', {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
        });

        this.router.navigate([`/subject/list`]); // Navigate to the edit page
        },
        (error) => {
          console.error('Error creating subject:', error);

          this.snackBar.open('Error, subject was not created!', 'Close', {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
        });
        }
      );
    }
  }

}

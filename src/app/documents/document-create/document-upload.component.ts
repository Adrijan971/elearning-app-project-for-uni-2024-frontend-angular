import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { Router, RouterModule } from '@angular/router';
import { MatSnackBarModule, MatSnackBar } from '@angular/material/snack-bar';
import { DocumentService } from '../../services/document.service';  // Assuming you have this service

@Component({
  selector: 'app-document-upload',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatDatepickerModule,
    MatToolbarModule,
    MatNativeDateModule,
    MatIconModule,
    RouterModule,
    MatSnackBarModule,
  ],
  templateUrl: './document-upload.component.html',
  styleUrls: ['./document-upload.component.css']
})
export class DocumentUploadComponent {
  documentForm: FormGroup;
  selectedFile: File | null = null;

  constructor(
    private fb: FormBuilder,
    private documentService: DocumentService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {
    this.documentForm = this.fb.group({
      documentName: ['', Validators.required],
    });
  }

  onFileSelected(event: Event): void {
    const input = event.target as HTMLInputElement;
    if (input.files && input.files.length > 0) {
      this.selectedFile = input.files[0];
    }
  }

  onSubmit(): void {
    if (this.documentForm.valid && this.selectedFile) {
      const documentName = this.documentForm.get('documentName')?.value;

      const currentStudentId: number = Number(localStorage.getItem("userId"));

      // Create form data for the upload request
      const formData = new FormData();
      formData.append('file', this.selectedFile);
      formData.append('name', documentName);
      formData.append('student_id', currentStudentId.toString());  // Replace with actual student ID

      // Call the service to upload the document
      this.documentService.uploadDocument(formData).subscribe(
        (response) => {
          console.log('Document uploaded successfully:', response);

          // Show success message
          this.snackBar.open('Document uploaded successfully!', 'Close', {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
          });

          // Navigate to another page (optional)
          this.router.navigate([`/myDocuments/list`]);  // Update to appropriate route
        },
        (error) => {
          console.error('Error uploading document:', error);

          // Show error message
          this.snackBar.open('Error uploading document!', 'Close', {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
          });
        }
      );
    }
  }
}

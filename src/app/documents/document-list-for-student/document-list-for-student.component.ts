import { Component, OnInit } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { RouterModule, Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { SubjectService } from '../../services/subject.service';
import { Subject } from '../../models/subject.model';
import { StudentService } from '../../services/student.service';
import { DocumentWithNames } from '../../models/documentWithNames';
import { DocumentService } from '../../services/document.service';

@Component({
  selector: 'app-subject-list',
  standalone: true,
  imports: [
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    RouterModule,
    CommonModule,
    MatToolbarModule
  ],
  templateUrl: './document-list-for-student.component.html',
  styleUrl: './document-list-for-student.component.css'
})
export class DocumentListForStudentComponent implements OnInit{

  documents: DocumentWithNames[] = [];
  displayedColumns: string[] = ['name', 'student', 'actions'];

  currentStudentId: number = Number(localStorage.getItem("userId"));

  constructor(
    private documentService: DocumentService, // Inject the service
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loadDocuments();
  }

  loadDocuments(): void {
    
    this.documentService.getDocumentsForStudent(this.currentStudentId).subscribe(
      (data: DocumentWithNames[]) => {
        this.documents = data;
      },
      (error) => {
        console.error('Failed to load documents', error);
      }
    );

  }

  downloadDocument(documentPath: string): void {
    const documentName = documentPath.split('/').pop() || '';
    // const documentName = "wallpaperflare.com_wallpaper.jpg";
    
    if (documentName) {
      this.documentService.downloadDocument(documentName).subscribe(
        (blob) => {
          // Create a URL for the file
          const url = window.URL.createObjectURL(blob);
          const a = document.createElement('a');
          a.href = url;
          a.download = documentName; // Use the extracted file name for download
          a.click(); // Trigger the download
          window.URL.revokeObjectURL(url); // Revoke the URL after download
        },
        (error) => {
          console.error('File download failed', error);
        }
      );
    } else {
      console.error('Invalid document path');
    }
  }


}

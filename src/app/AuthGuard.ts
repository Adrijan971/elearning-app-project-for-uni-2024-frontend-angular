import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {
    if (this.authService.isAuthenticated()) {

      // Get the required role from the route data
      const allowedRoles = route.data['roles'] as Array<string>;

      const userRole = localStorage.getItem('userRole'); 
    
      // Check if the user's role matches the required role
      if (userRole && allowedRoles.includes(userRole)) {
        return true; // Allow access if the user's role matches
      } else {
        // Redirect to unauthorized page or login
        this.router.navigate(['/unauthorized']);  // You can create this route for unauthorized access
        return false;
      }
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
}

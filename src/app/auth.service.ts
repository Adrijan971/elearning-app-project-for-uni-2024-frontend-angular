import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService  {

  private baseUrl: string = 'http://localhost:8080';

  private isAuthenticatedSubject = new BehaviorSubject<boolean>(this.isAuthenticated());

  // Expose the authentication status as an observable
  isAuthenticated$ = this.isAuthenticatedSubject.asObservable();

  constructor(private http: HttpClient) {}

  getAuthToken(): string | null {
    const token = window.localStorage.getItem('auth_token');
    if (token && this.isTokenExpired(token)) {
      this.setAuthToken(null); // Remove the expired token
      return null;
    }
    return token;
  }

  // Autentifikuje ili brise token, sve zavisi sta mu se posalje
  setAuthToken(token: string | null): void {
    if (token !== null) {
      window.localStorage.setItem('auth_token', token);
      this.isAuthenticatedSubject.next(true);  // Notify subscribers that user is authenticated
    } else {
      window.localStorage.removeItem('auth_token');
      this.isAuthenticatedSubject.next(false);  // Notify subscribers that user is logged out
    }
  }

  request(method: string, url: string, data: any): Observable<any> {
    const fullUrl = `${this.baseUrl}${url}`;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });

    // Choose the correct HTTP method and call HttpClient accordingly
    if (method.toLowerCase() === 'post') {
      return this.http.post(fullUrl, data, { headers });
    } else if (method.toLowerCase() === 'get') {
      return this.http.get(fullUrl, { headers });
    } else if (method.toLowerCase() === 'put') {
      return this.http.put(fullUrl, data, { headers });
    } else if (method.toLowerCase() === 'delete') {
      return this.http.delete(fullUrl, { headers });
    } else {
      throw new Error(`Unsupported HTTP method: ${method}`);
    }
  }

  // Helper function to decode and check if the token is expired
  isTokenExpired(token: string): boolean {
    const payload = JSON.parse(atob(token.split('.')[1])); // Decode the payload from the JWT
    const expiry = payload.exp * 1000; // Convert the expiration time to milliseconds
    return Date.now() > expiry; // Return true if the current time is greater than the expiration time
  }

  getCurrentUserRole(): string | null{
    return window.localStorage.getItem('userRole');
  }

  isAuthenticated(): boolean {
    const token = this.getAuthToken();
    return token !== null;  // Returns true if a valid token exists
  }

  logout(): void {
    this.setAuthToken(null);
    // this.router.navigate(['/login']); // Redirect the user to the login page
  }

}

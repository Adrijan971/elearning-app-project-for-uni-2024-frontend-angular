import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
// import { MatErrorModule } from '@angular/material/form-field';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { SubjectService } from '../../services/subject.service';
import { TeacherService } from '../../services/teacher.service';
import { ExamService } from '../../services/exam.service';
import { ExamCreateDTO } from '../../DTOs/createDTOs/examCreateDTO.model';
import { MatSnackBarModule, MatSnackBar } from '@angular/material/snack-bar';
import { Router, RouterModule } from '@angular/router';

import { CommonModule } from '@angular/common';
import { LinkTeacherSubjectDTO } from '../../DTOs/LinkDTOs/LinkTeacherSubject.model';

@Component({
  selector: 'app-exam-create',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatDatepickerModule,
    MatToolbarModule,
    MatNativeDateModule,
    MatIconModule,
    MatSelectModule,
    CommonModule,
    MatSnackBarModule,
    RouterModule
  ],
  templateUrl: './assign-teacher-subject.component.html',
  styleUrl: './assign-teacher-subject.component.css'
})
export class AssignTeacherSubjectComponent implements OnInit{

  linkForm: FormGroup;
  submitted = false;

  subjects: any[] = [];
  teachers: any[] = [];

  constructor(
    private fb: FormBuilder, 
    private teacherService: TeacherService, 
    private subjectService: SubjectService,
    private snackBar: MatSnackBar,
    private router: Router

  ) {
    this.linkForm = this.fb.group({
      subject: ['', Validators.required],
      teacher: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.subjectService.getSubjects().subscribe((data: any[]) => {
      this.subjects = data;
    });
    this.teacherService.getTeachers().subscribe((data: any[]) => {
      this.teachers = data;
    });
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.linkForm.valid) {
   
      const linkDTO: LinkTeacherSubjectDTO = {
        teacherId: this.linkForm.get('teacher')?.value,
        subjectId: this.linkForm.get('subject')?.value,
       
      };

      this.teacherService.linkToSubject(linkDTO).subscribe(
        (response) => {
          console.log('Linked to subject successfully:', response);

        this.snackBar.open('Linked to subject successfully!', 'Close', {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
        });

        this.router.navigate([`/`]);
        },
        (error) => {
          console.error('Error creating a link:', error);

          this.snackBar.open('Error, link was not created!', 'Close', {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
        });
        }
      );
    }
  }
}

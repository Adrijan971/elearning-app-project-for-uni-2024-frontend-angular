import { Component, OnInit } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { RouterModule, Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { PaymentService } from '../../services/payment.service';
import { PaymentWithNames } from '../../models/paymentWithNames.model';
import { StudentService } from '../../services/student.service';
// import { ExamUpdateDTO } from '../../DTOs/updateDTOs/examUpdateDTO.model';

@Component({
  selector: 'app-payment-list',
  standalone: true,
  imports: [
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    RouterModule,
    CommonModule,
    MatToolbarModule
  ],
  templateUrl: './my-payment-list.component.html',
  styleUrl: './my-payment-list.component.css'
})
export class MyPaymentListComponent implements OnInit{

  payments: PaymentWithNames[] = [];
  displayedColumns: string[] = ['bankAccount', 'amount', 'studentName', 'dateTime'];

  constructor(
    private paymentService: PaymentService,
    private studentService: StudentService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loadPayments();
  }

  loadPayments(): void {
    this.studentService.getPaymentsForStudent(Number(localStorage.getItem("userId"))).subscribe(
      (data: PaymentWithNames[]) => {
        this.payments = data;

        this.payments.forEach((payment) => {
          payment.dateTime = payment.dateTime.split("T")[0];
        }); 
      },
      (error) => {
        console.error('Failed to load payments', error);
      }
    );

  }

  editPayment(id: number): void {
    this.router.navigate([`/payment/edit/${id}`]);
  }

  deletePayment(id: number): void {
    this.paymentService.deletePayment(id).subscribe(
      () => {
        console.log('Deleted payment with ID:', id);
        this.loadPayments(); // Reload payments after deletion
      },
      (error) => {
        console.error('Failed to delete payment', error);
      }
    );
  }

}

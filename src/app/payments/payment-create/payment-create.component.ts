import { Component, OnInit } from '@angular/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule } from '@angular/material/core';
// import { MatErrorModule } from '@angular/material/form-field';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { PaymentService } from '../../services/payment.service';
import { StudentService } from '../../services/student.service';
import { PaymentCreateDTO } from '../../DTOs/createDTOs/paymentCreateDTO.model';
import { Router, RouterModule } from '@angular/router';
import { MatSnackBarModule, MatSnackBar } from '@angular/material/snack-bar';
import { MatSelectModule } from '@angular/material/select';
import { CommonModule } from '@angular/common';



@Component({
  selector: 'app-payment-create',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatDatepickerModule,
    MatToolbarModule,
    MatNativeDateModule,
    MatIconModule,
    RouterModule,
    MatSnackBarModule,
    MatSelectModule,
    CommonModule,
  ],
  templateUrl: './payment-create.component.html',
  styleUrl: './payment-create.component.css'
})
export class PaymentCreateComponent implements OnInit {

  paymentForm: FormGroup;
  submitted = false;

  students: any[] = [];

  constructor(
    private fb: FormBuilder,
    private paymentService: PaymentService,
    private studentService: StudentService,
    private router: Router,
    private snackBar: MatSnackBar,
   ) {
    this.paymentForm = this.fb.group({
      bankAccount: ['', Validators.required],
      amount: ['', [Validators.required, Validators.min(1)]],
      student: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.studentService.getStudents().subscribe((data: any[]) => {
      this.students = data;
    });
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.paymentForm.valid) {

      const now = new Date();
      const dateTime = now.toISOString().split('.')[0]; // Remove milliseconds

      const paymentDTO: PaymentCreateDTO = {
        bankAccount: this.paymentForm.get('bankAccount')?.value,
        amount: this.paymentForm.get('amount')?.value,
        dateTime: dateTime,
        studentId: this.paymentForm.get('student')?.value,

      };

      // Call the service to create the Payment
      this.paymentService.createPayment(paymentDTO).subscribe(
        (response) => {
          console.log('Payment created successfully:', response);

        this.snackBar.open('Payment created successfully!', 'Close', {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
        });

        this.router.navigate([`/payment/list`]);
        },
        (error) => {
          console.error('Error creating payment:', error);

          this.snackBar.open('Error, payment was not created!', 'Close', {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
        });
        }
      );
    }
  }

}

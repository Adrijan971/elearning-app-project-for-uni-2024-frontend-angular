import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule, MatSnackBar } from '@angular/material/snack-bar';
import { PaymentService } from '../../services/payment.service';
import { Payment } from '../../models/payment.model';
import { PaymentUpdateDTO } from '../../DTOs/updateDTOs/paymentUpdateDTO.model';

@Component({
  selector: 'app-payment-edit',
  standalone: true,
  imports: [
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatDividerModule,
    CommonModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatIconModule
  ],
  templateUrl: './payment-edit.component.html',
  styleUrl: './payment-edit.component.css'
})
export class PaymentEditComponent implements OnInit{

  paymentForm!: FormGroup;
  paymentId!: number;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private paymentService: PaymentService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {

    // Initialize the form
    this.paymentForm = this.fb.group({
      amount: ['', Validators.required],
    });

    // Get subject ID from the route
    this.paymentId = +this.route.snapshot.paramMap.get('id')!;
    this.loadPaymentData(this.paymentId);
  }

  loadPaymentData(id: number): void {
    this.paymentService.getPayment(id).subscribe(
      (payment: Payment) => {
        this.paymentForm.get('amount')?.setValue(payment.amount);
      },
      (error) => {
        console.error('Failed to load payment', error);
      }
    );
  }

  onSubmit(): void {
    if (this.paymentForm.valid) {

      let amount = this.paymentForm.get('amount')?.value;

      const updatedPayment: PaymentUpdateDTO = {
        amount : this.paymentForm.get('amount')?.value,
      };

      this.paymentService.updatePayment(this.paymentId, updatedPayment).subscribe(
        (response) => {
          console.log('Payment updated successfully:', response);

          this.snackBar.open('Payment updated successfully!', 'Close', {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
          });
          this.router.navigate(['payment/list']);
        },
        (error) => {
          console.error('Error updating Payment:', error);

          this.snackBar.open('Error, Payment was not updated!', 'Close', {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
        })}
      );
    }
  }

  onCancel(): void {
    this.router.navigate(['payment/list']);
  }
}

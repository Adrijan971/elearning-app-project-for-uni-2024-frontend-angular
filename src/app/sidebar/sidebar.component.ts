import { Component, ViewChild, Input, SimpleChanges  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, RouterOutlet } from '@angular/router';
import { HeaderComponent } from '../header/header.component';
import { MatSidenavModule, MatDrawer  } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MainLoginFormComponent } from '../loginForms/main-login-form/main-login-form.component';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-sidebar',
  standalone: true,
  imports: [
    RouterOutlet,
    RouterModule,
    MainLoginFormComponent,
    HeaderComponent,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    CommonModule,
  ],
  templateUrl: './sidebar.component.html',
  styleUrl: './sidebar.component.css'
})
export class SidebarComponent {

  @ViewChild('drawer') drawer!: MatDrawer;

  constructor(private authService : AuthService){
    this.userRole = this.authService.getCurrentUserRole() || "";
  }

  isAuthenticated: boolean = false;

  userRole: string = '';

  // every time button is clicked in header it will send new value and we 'catch' changes.
  @Input() action: number = 0;

  isAdministrator(): boolean {
    return this.userRole === 'ADMINISTRATOR';
  }

  isTeacher(): boolean {
    return this.userRole === 'TEACHER';
  }

  isStudent(): boolean {
    return this.userRole === 'STUDENT';
  }


  ngOnChanges(changes: SimpleChanges) {

    // Subscribe to authentication changes
    this.authService.isAuthenticated$.subscribe((authStatus: boolean) => {
      this.isAuthenticated = authStatus;
    });

    if (changes['action']) {

      // Call the function to wait for the drawer
      this.waitForDrawer().then(() => {
        this.drawer.toggle();
      }).catch((err) => {
        console.log("Error: ", err);
      });
    }
  }

  // Create a function to manually wait for drawer initialization
  waitForDrawer(): Promise<void> {
    return new Promise((resolve, reject) => {
      const intervalId = setInterval(() => {
        if (this.drawer) {
          clearInterval(intervalId); // Stop polling once drawer is available
          resolve();
        }
      }, 100);

      // Set a timeout to reject the promise if drawer isn't initialized after 5 seconds
      setTimeout(() => {
        clearInterval(intervalId);
      }, 5000);
    });
  }



}

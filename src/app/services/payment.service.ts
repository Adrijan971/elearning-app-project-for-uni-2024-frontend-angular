import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Payment } from '../models/payment.model';
import { PaymentWithNames } from '../models/paymentWithNames.model'; 
import { PaymentCreateDTO } from '../DTOs/createDTOs/paymentCreateDTO.model';
import { PaymentUpdateDTO } from '../DTOs/updateDTOs/paymentUpdateDTO.model';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  private apiUrl = 'http://localhost:8080/api/payments';

  constructor(private http: HttpClient) {}

  // Get all payments
  getPayments(): Observable<Payment[]> {
    return this.http.get<Payment[]>(this.apiUrl);
  }

  // Get all payments with names not with ids
  getPaymentsWithNames(): Observable<PaymentWithNames[]> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `${this.apiUrl}/listWithNames`;
    return this.http.get<PaymentWithNames[]>(url);
  }

  // Get a Payment by ID
  getPayment(id: number): Observable<Payment> {
    const url = `${this.apiUrl}/id/${id}`;
    return this.http.get<Payment>(url);
  }

  // Create a new Payment
  createPayment(paymentDto: PaymentCreateDTO): Observable<Payment> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `${this.apiUrl}/create`;
    return this.http.post<Payment>(url, paymentDto, { headers });
  }

  // Update an existing Payment
  updatePayment(id: number, paymentDto: PaymentUpdateDTO): Observable<Payment> {
    const url = `${this.apiUrl}/update/${id}`;
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put<Payment>(url, paymentDto, { headers });
  }

  // Delete a Payment by ID
  deletePayment(id: number): Observable<void> {
    const url = `${this.apiUrl}/archive/${id}`;
    return this.http.delete<void>(url);
  }
}

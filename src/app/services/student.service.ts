import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Student } from '../models/student.model'; 
import { StudentCreateDTO } from '../DTOs/createDTOs/studentCreateDTO.model';
import { StudentUpdateDTO } from '../DTOs/updateDTOs/studentUpdateDTO.model';
import { Subject } from '../models/subject.model';
import { PaymentWithNames } from '../models/paymentWithNames.model';
import { LinkStudentSubjectDTO } from '../DTOs/LinkDTOs/LinkStudentSubject.model';
import { ExamWithNames } from '../models/examWithNames';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  private apiUrl = 'http://localhost:8080/api/students'; // Replace with your actual API URL

  constructor(private http: HttpClient) {}

  // Get all students
  getStudents(): Observable<Student[]> {
    return this.http.get<Student[]>(this.apiUrl);
  }

  // Get all subjects per sudentId
  getSubjectsForStudent(studentId: number): Observable<Subject[]> {
    const url = `${this.apiUrl}/subjectsByStudentId/${studentId}`;
    return this.http.get<Subject[]>(url);
  }

  // Get all payments per sudentId
  getPaymentsForStudent(studentId: number): Observable<PaymentWithNames[]> {
    const url = `${this.apiUrl}/paymentsByStudentId/${studentId}`;
    return this.http.get<PaymentWithNames[]>(url);
  }

  // Get all exams per studentId
  getExamsForStudent(studentId: number): Observable<ExamWithNames[]> {
    const url = `${this.apiUrl}/examsByStudentId/${studentId}`;
    return this.http.get<ExamWithNames[]>(url);
  }

  // Get a student by ID
  getStudent(id: number): Observable<Student> {
    const url = `${this.apiUrl}/id/${id}`;
    return this.http.get<Student>(url);
  }

  // Create a new student
  createStudent(student: StudentCreateDTO): Observable<Student> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `http://localhost:8080/register`;
    return this.http.post<Student>(url, student, { headers });
  }

  // Link student to subject
  linkToSubject(linkDTO: LinkStudentSubjectDTO): Observable<string> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `${this.apiUrl}/linkSubjectToStudent/${linkDTO.studentId}/${linkDTO.subjectId}`;
    return this.http.post<string>(url, { headers });
  }

  // Update an existing student
  updateStudent(id: number, student: StudentUpdateDTO): Observable<Student> {
    const url = `${this.apiUrl}/update/${id}`;
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put<Student>(url, student, { headers });
  }

  // Delete a student by ID
  deleteStudent(id: number): Observable<void> {
    const url = `${this.apiUrl}/archive/${id}`;
    return this.http.delete<void>(url);
  }
}

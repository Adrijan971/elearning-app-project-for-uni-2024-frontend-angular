import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Subject } from '../models/subject.model'; 
import { SubjectCreateDTO } from '../DTOs/createDTOs/subjectCreateDTO.model';
import { SubjectUpdateDTO } from '../DTOs/updateDTOs/subjectUpdateDTO.model';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {

  private apiUrl = 'http://localhost:8080/api/subjects';

  constructor(private http: HttpClient) {}

  // Get all subjects
  getSubjects(): Observable<Subject[]> {
    return this.http.get<Subject[]>(this.apiUrl);
  }

  // Get a subject by ID
  getSubject(id: number): Observable<Subject> {
    const url = `${this.apiUrl}/id/${id}`;
    return this.http.get<Subject>(url);
  }

  // Create a new subject
  createSubject(subjectDto: SubjectCreateDTO): Observable<Subject> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `${this.apiUrl}/create`;
    return this.http.post<Subject>(url, subjectDto, { headers });
  }

  // Update an existing subject
  updateSubject(id: number, subject: SubjectUpdateDTO): Observable<Subject> {
    const url = `${this.apiUrl}/update/${id}`;
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put<Subject>(url, subject, { headers });
  }

  // Delete a subject by ID
  deleteSubject(id: number): Observable<void> {
    const url = `${this.apiUrl}/archive/${id}`;
    return this.http.delete<void>(url);
  }

}

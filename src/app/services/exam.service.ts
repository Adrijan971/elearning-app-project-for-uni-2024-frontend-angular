import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ExamWithNames } from '../models/examWithNames'; 
import { Exam } from '../models/exam.model'; 
import { ExamUpdateDTO } from '../DTOs/updateDTOs/examUpdateDTO.model';
import { ExamCreateDTO } from '../DTOs/createDTOs/examCreateDTO.model';


@Injectable({
  providedIn: 'root'
})
export class ExamService {
  private apiUrl = 'http://localhost:8080/api/exams';

  constructor(private http: HttpClient) {}

  // Get all exams with names not with ids
  getExamsWithNames(): Observable<ExamWithNames[]> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `${this.apiUrl}/listWithNames`;
    return this.http.get<ExamWithNames[]>(url);
  }

  // Get all exams with names per teacher
  getExamsWithNamesForTeacher(id: number): Observable<ExamWithNames[]> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `${this.apiUrl}/listPerTeacherWithNames/${id}`;
    return this.http.get<ExamWithNames[]>(url);
  }

  // Get a exam by ID
  getExam(id: number): Observable<Exam> {
    const url = `${this.apiUrl}/id/${id}`;
    return this.http.get<Exam>(url);
  }

    // Get a exam by ID with names
    getExamWithNames(id: number): Observable<Exam> {
      const url = `${this.apiUrl}/id-with-names/${id}`;
      return this.http.get<Exam>(url);
    }

  // Create a new exam
  createExam(examtDto: ExamCreateDTO): Observable<Exam> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `${this.apiUrl}/create`;
    return this.http.post<Exam>(url, examtDto, { headers });
  }

  // Update an existing exam
  updateExam(id: number, exam: ExamUpdateDTO): Observable<Exam> {
    const url = `${this.apiUrl}/update/${id}`;
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put<Exam>(url, exam, { headers });
  }

  // Delete a exam by ID
  deleteExam(id: number): Observable<void> {
    const url = `${this.apiUrl}/archive/${id}`;
    return this.http.delete<void>(url);
  }
}

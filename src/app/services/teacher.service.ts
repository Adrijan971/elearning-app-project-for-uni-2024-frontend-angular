import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Teacher } from '../models/teacher.model'; 
import { TeacherCreateDTO } from '../DTOs/createDTOs/teacherCreateDTO.model';
import { TeacherUpdateDTO } from '../DTOs/updateDTOs/teacherUpdateDTO.model';
import { Subject } from '../models/subject.model';
import { LinkTeacherSubjectDTO } from '../DTOs/LinkDTOs/LinkTeacherSubject.model';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {

  private apiUrl = 'http://localhost:8080/api/teachers';

  constructor(private http: HttpClient) {}

  // Get all Teachers
  getTeachers(): Observable<Teacher[]> {
    return this.http.get<Teacher[]>(this.apiUrl);
  }

  // Get all subjects per teacherId
  getSubjectsForTeacher(teacherId: number): Observable<Subject[]> {
    const url = `${this.apiUrl}/subjectsByTeacherId/${teacherId}`;
    return this.http.get<Subject[]>(url);
  }

  // Create a new teacher
  createTeacher(teacherCreateDTO: TeacherCreateDTO): Observable<Teacher> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `http://localhost:8080/register`;
    return this.http.post<Teacher>(url, teacherCreateDTO, { headers });
  }

  // Link teacher to subject
  linkToSubject(linkDTO: LinkTeacherSubjectDTO): Observable<string> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `${this.apiUrl}/linkSubjectToTeacher/${linkDTO.teacherId}/${linkDTO.subjectId}`;
    return this.http.post<string>(url, { headers });
  }

  // Get a teacher by ID
  getTeacher(id: number): Observable<Teacher> {
    const url = `${this.apiUrl}/id/${id}`;
    return this.http.get<Teacher>(url);
  }

  // Update an existing teacher
  updateTeacher(id: number, teacher: TeacherUpdateDTO): Observable<Teacher> {
    const url = `${this.apiUrl}/update/${id}`;
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put<Teacher>(url, teacher, { headers });
  }

  // Delete a teacher by ID
  deleteTeacher(id: number): Observable<void> {
    const url = `${this.apiUrl}/archive/${id}`;
    return this.http.delete<void>(url);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DocumentWithNames } from '../models/documentWithNames';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  private apiUrl = 'http://localhost:8080/api/documents';

  constructor(private http: HttpClient) {}

  // Get all documents with names not with ids
  getDocumentsWithNames(): Observable<DocumentWithNames[]> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `${this.apiUrl}/listWithNames`;
    return this.http.get<DocumentWithNames[]>(url);
  }

  // Get all documents per sudentId
  getDocumentsForStudent(studentId: number): Observable<DocumentWithNames[]> {
    const url = `${this.apiUrl}/listPerStudentWithNames/${studentId}`;
    return this.http.get<DocumentWithNames[]>(url);
  }

  uploadDocument(formData: FormData): Observable<any> {
    const url = `${this.apiUrl}/upload`;
    const headers = new HttpHeaders({
      'Accept': 'application/json',
    });

    return this.http.post(url, formData, { headers });
  }

  downloadDocument(documentName: string): Observable<Blob> {
    const url = `${this.apiUrl}/download/${documentName}`;
    
    // Set custom headers if needed
    const headers = new HttpHeaders({
      'Accept': 'application/octet-stream', // Specifies the type of content expected
      'Content-Disposition': `attachment; filename=${documentName}`, // Suggests filename to the browser
    });

    // Specify that the response type is 'blob'
    return this.http.get(url, { headers, responseType: 'blob' });
  }

}

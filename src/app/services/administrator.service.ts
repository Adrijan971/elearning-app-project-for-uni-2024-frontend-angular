import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Administrator } from '../models/administrator.model'; 
import { AdministratorCreateDTO } from '../DTOs/createDTOs/administratorCreateDTO.model';
import { AdministratorUpdateDTO } from '../DTOs/updateDTOs/administratorUpdateDTO.model';


@Injectable({
  providedIn: 'root'
})
export class AdministratorService {

  private apiUrl = 'http://localhost:8080/api/administrators';

  constructor(private http: HttpClient) {}

  // Get all subjects
  getAdministrators(): Observable<Administrator[]> {
    return this.http.get<Administrator[]>(this.apiUrl);
  }

  // Get a administrator by ID
  getAdministrator(id: number): Observable<Administrator> {
    const url = `${this.apiUrl}/id/${id}`;
    return this.http.get<Administrator>(url);
  }

  // Create a new administrator
  createStudent(administratorCreateDTO: AdministratorCreateDTO): Observable<Administrator> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `http://localhost:8080/register`;
    return this.http.post<Administrator>(url, administratorCreateDTO, { headers });
  }

  // Update an existing administrator
  updateAdministrator(id: number, administrator: AdministratorUpdateDTO): Observable<Administrator> {
    const url = `${this.apiUrl}/update/${id}`;
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put<Administrator>(url, administrator, { headers });
  }

  // Delete a administrator by ID
  deleteAdministrator(id: number): Observable<void> {
    const url = `${this.apiUrl}/archive/${id}`;
    return this.http.delete<void>(url);
  }

}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApplicationCreateDTO } from '../DTOs/createDTOs/applicationCreateDTO.model';
import { Application } from '../models/application.model';
import { ApplicationWithNames } from '../models/applicationWithNames.model';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  private apiUrl = 'http://localhost:8080/api/applications';

  constructor(private http: HttpClient) {}

  // Get all applications
  getApplications(): Observable<Application[]> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `${this.apiUrl}/list`;
    return this.http.get<Application[]>(url);
  }

  // Get all applications with names not with ids
  getApplicationsWithNames(): Observable<ApplicationWithNames[]> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `${this.apiUrl}/listWithNames`;
    return this.http.get<ApplicationWithNames[]>(url);
  }

  // Get all applications per examId
  getApplicationsForExam(examId: number): Observable<ApplicationWithNames[]> {
    const url = `${this.apiUrl}/listPerExamWithNames/${examId}`;
    return this.http.get<ApplicationWithNames[]>(url);
  }

  // Get all applications per sudentId
  getApplicationsForStudent(studentId: number): Observable<ApplicationWithNames[]> {
    const url = `${this.apiUrl}/listPerStudentWithNames/${studentId}`;
    return this.http.get<ApplicationWithNames[]>(url);
  }

  // Get all applications per teacherId
  getApplicationsForTeacher(teacherId: number): Observable<ApplicationWithNames[]> {
    const url = `${this.apiUrl}/listPerTeacherWithNames/${teacherId}`;
    return this.http.get<ApplicationWithNames[]>(url);
  }

  // Create a new Application
  createApplication(applicationDto: ApplicationCreateDTO): Observable<Application> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `${this.apiUrl}/create`;

    return this.http.post<Application>(url, applicationDto, { headers });
  }

  // Pass an application by ID
  passApplication(id: number, grade: number): Observable<void> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `${this.apiUrl}/pass/${id}/${grade}`;
    return this.http.put<void>(url, headers);
  }

  // Delete applciation by id
  archiveApplication(id: number): Observable<void> {
    const url = `${this.apiUrl}/archive/${id}`;
    return this.http.delete<void>(url);
  }

}

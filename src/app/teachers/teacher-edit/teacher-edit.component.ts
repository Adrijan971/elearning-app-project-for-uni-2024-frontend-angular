import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { TeacherService } from '../../services/teacher.service'; // Import the service
import { TeacherUpdateDTO } from '../../DTOs/updateDTOs/teacherUpdateDTO.model';
import { MatSnackBarModule, MatSnackBar } from '@angular/material/snack-bar';
import { Teacher } from '../../models/teacher.model'; // Adjust the path according to your project


@Component({
  selector: 'app-teacher-edit',
  standalone: true,
  imports: [
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatDividerModule,
    CommonModule,
    ReactiveFormsModule, // Import the ReactiveFormsModule for form handling
    MatToolbarModule,
    MatIconModule
  ],
  templateUrl: './teacher-edit.component.html',
  styleUrl: './teacher-edit.component.css'
})
export class TeacherEditComponent implements OnInit {

  teacherForm!: FormGroup;
  teacherId!: number;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private teacherService: TeacherService,
    private snackBar: MatSnackBar

  ) {}

  ngOnInit(): void {
    // Initialize the form
    this.teacherForm = this.fb.group({
      username: ['', [Validators.required, Validators.email]],
      name: ['', Validators.required],
      lastname: ['', Validators.required],
      address: ['', Validators.required],
      mobilePhone: ['', Validators.required],
    });

    // Get teacher ID from the route (if necessary)
    this.teacherId = +this.route.snapshot.paramMap.get('id')!;
    // Fetch the teacher data and patch the form (replace with actual service call)
    this.loadTeacherData(this.teacherId);
  }

  loadTeacherData(id: number) {
    this.teacherService.getTeacher(id).subscribe(
      (teacher: Teacher) => {
        this.teacherForm.patchValue(teacher);
      },
      (error) => {
        console.error('Failed to load teacher', error);
      }
    );

  }

  onSubmit() {
    if (this.teacherForm.valid) {
      const updatedTeacher: TeacherUpdateDTO = this.teacherForm.value;
      this.teacherService.updateTeacher(this.teacherId, updatedTeacher).subscribe(
        (response) => {
          console.log('Teacher updated successfully:', response);

          this.snackBar.open('Teacher updated successfully!', 'Close', {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
          });
          this.router.navigate(['teacher/list']); // Navigate to the teacher list after saving
        },
        (error) => {
          console.error('Error creating Teacher:', error);

          this.snackBar.open('Error, Teacher was not updated!', 'Close', {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
        })}
      );
    }
  }

  onCancel() {
    this.router.navigate(['teacher/list']); // Navigate back on cancel
  }

}

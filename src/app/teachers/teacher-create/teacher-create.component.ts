import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { TeacherService } from '../../services/teacher.service';
import { TeacherCreateDTO } from '../../DTOs/createDTOs/teacherCreateDTO.model';
import { MatSnackBarModule, MatSnackBar } from '@angular/material/snack-bar';
import { Router, RouterModule } from '@angular/router';
import { MatSelectModule } from '@angular/material/select';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-teacher-create',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatDatepickerModule,
    MatToolbarModule,
    MatNativeDateModule,
    MatIconModule,
    MatSelectModule,
    CommonModule
  ],
  templateUrl: './teacher-create.component.html',
  styleUrl: './teacher-create.component.css'
})
export class TeacherCreateComponent {

  teacherForm: FormGroup;
  submitted = false;

  constructor(private fb: FormBuilder, private teacherService: TeacherService, private router: Router,  private snackBar: MatSnackBar ) {
    this.teacherForm = this.fb.group({
      firstname: ['', Validators.required],
      lastname:  ['', Validators.required],
      username:  ['', [Validators.required, Validators.pattern(/@/)]],
      password:  ['', Validators.required],
      teacherType:      ['', Validators.required],
    });
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.teacherForm.valid) {
      const teacherCreateDTO: TeacherCreateDTO = {
        firstName: this.teacherForm.get('firstname')?.value,
        lastName: this.teacherForm.get('lastname')?.value,
        username: this.teacherForm.get('username')?.value,
        password: this.teacherForm.get('password')?.value,
        userType: "TEACHER",
        teacherType: this.teacherForm.get('teacherType')?.value,
      };

      this.teacherService.createTeacher(teacherCreateDTO).subscribe(
        (response) => {
          console.log('Teacher created successfully:', response);

        this.snackBar.open('Teacher created successfully!', 'Close', {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
        });

        this.router.navigate([`/teacher/list`]);
        },
        (error) => {
          console.error('Error creating Teacher:', error);

          this.snackBar.open('Error, Teacher was not created!', 'Close', {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
        });
        }
      );
    }
  }

}

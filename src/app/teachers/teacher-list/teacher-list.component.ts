import { Component } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { RouterModule, Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { Teacher } from '../../models/teacher.model';
import { TeacherService } from '../../services/teacher.service';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-teacher-list',
  standalone: true,
  imports: [
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    RouterModule,
    CommonModule,
    MatToolbarModule
  ],
  templateUrl: './teacher-list.component.html',
  styleUrl: './teacher-list.component.css'
})
export class TeacherListComponent {

  teachers: Teacher[] = [];
  displayedColumns: string[] = ['id', 'username', 'name','lastname', 'archived', 'actions'];

  userRole: string = '';

  constructor(
    private teacherService: TeacherService,
    private router: Router,
    private authService : AuthService
  ) {
    this.userRole = this.authService.getCurrentUserRole() || "";
  }

  isAdministrator(): boolean {
    return this.userRole === 'ADMINISTRATOR';
  }

  isTeacher(): boolean {
    return this.userRole === 'TEACHER';
  }

  isStudent(): boolean {
    return this.userRole === 'STUDENT';
  }

  ngOnInit(): void {
    this.loadTeachers();
  }

  loadTeachers(): void {
    this.teacherService.getTeachers().subscribe(
      (data: Teacher[]) => {
        this.teachers = data;
      },
      (error) => {
        console.error('Failed to load teachers', error);
      }
    );

  }

  editTeacher(id: number): void {
    this.router.navigate([`/teacher/edit/${id}`]);
  }

  deleteTeacher(id: number): void {
    this.teacherService.deleteTeacher(id).subscribe(
      () => {
        console.log('Deleted teacher with ID:', id);
        this.loadTeachers(); // Reload teachers after deletion
      },
      (error) => {
        console.error('Failed to delete teacher', error);
      }
    );
  }

}

import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentLoginFormComponent } from '../student-login-form/student-login-form.component';
import { AdministratorLoginFormComponent } from '../administrator-login-form/administrator-login-form.component';
import { TeacherLoginFormComponent } from '../teacher-login-form/teacher-login-form.component';
import { AuthService } from '../../auth.service'
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-main-login-form',
  standalone: true,
  imports: [
    StudentLoginFormComponent,
    AdministratorLoginFormComponent,
    TeacherLoginFormComponent,
    CommonModule
  ],
  templateUrl: './main-login-form.component.html',
  styleUrl: './main-login-form.component.css'
})
export class MainLoginFormComponent implements OnInit{
  
  constructor(
    private authService: AuthService, 
    private router: Router,
    private snackBar: MatSnackBar,
  ){}


  ngOnInit(): void {
    // Nisam uradio ovaj check u AuthGuardu jer je dolazilo do neke cudne rekurzije
    if (this.authService.isAuthenticated() && this.router.url === '/login') {
      // Redirect to another route if authenticated
      this.router.navigate(['/subject/list']);
    }
  }

  @Output() loginSuccess = new EventEmitter<boolean>();

  selectedLogin: string = 'student';

  selectLogin(type: string) {
    this.selectedLogin = type;
  }

  onLoginEvent(event: { login: string; password: string; userType: string }): void {
    const { login, password, userType } = event;

    this.authService.request('POST', '/login', { username: login, password: password, userType: userType }).subscribe(
      (response: any) => {
        this.authService.setAuthToken(response.token); // setuj token ako ga ima u response

        localStorage.setItem('userId', response.id);
        localStorage.setItem('username', response.username);
        localStorage.setItem('userRole', response.role);
        localStorage.setItem('name', response.name);
        localStorage.setItem('lastname', response.lastname);
        localStorage.setItem('teacherRole', response.teacherRole);  // In case teacher logs in.

        this.loginSuccess.emit(true);
        // this.router.navigate(['/student/list']);  // ovo je navodno preporucljivo
        window.location.href = '/subject/list'; // refresh the page
      },
      (error) => {
        this.snackBar.open('Wrong credentials, try again!', 'Close', {
          duration: 3000,
          verticalPosition: 'top',
          horizontalPosition: 'right',
        });
        console.error('Login failed:', error);
      }
    );
  }
}

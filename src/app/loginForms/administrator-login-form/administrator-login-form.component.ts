import { Component, EventEmitter, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-administrator-login-form',
  standalone: true,
  imports: [FormsModule,CommonModule],
  templateUrl: './administrator-login-form.component.html',
  styleUrl: './administrator-login-form.component.css'
})
export class AdministratorLoginFormComponent {

  @Output() onSubmitLoginEvent = new EventEmitter();

  login: string = "";
  password: string = "";

  onSubmitLogin(): void {
    this.onSubmitLoginEvent.emit({"login": this.login, "password": this.password, "userType": "ADMINISTRATOR"});
  }

}

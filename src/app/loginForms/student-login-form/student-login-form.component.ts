import { Component, EventEmitter, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-student-login-form',
  standalone: true,
  imports: [FormsModule,CommonModule],
  templateUrl: './student-login-form.component.html',
  styleUrl: './student-login-form.component.css'
})
export class StudentLoginFormComponent {

  @Output() onSubmitLoginEvent = new EventEmitter();

  login: string = "";
  password: string = "";

  onSubmitLogin(): void {
    
    this.onSubmitLoginEvent.emit({"login": this.login, "password": this.password, "userType": "STUDENT"});
  }

}

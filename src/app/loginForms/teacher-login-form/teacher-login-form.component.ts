import { Component, EventEmitter, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-teacher-login-form',
  standalone: true,
  imports: [FormsModule,CommonModule],
  templateUrl: './teacher-login-form.component.html',
  styleUrl: './teacher-login-form.component.css'
})
export class TeacherLoginFormComponent {

  @Output() onSubmitLoginEvent = new EventEmitter();

  login: string = "";
  password: string = "";

  onSubmitLogin(): void {
    this.onSubmitLoginEvent.emit({"login": this.login, "password": this.password, "userType": "TEACHER"});
  }

}

export interface Exam {
  id: number;
  name: string;
  dateTime: string;
  teacherId: number;
  subjectId: number;
  archived: boolean;
}
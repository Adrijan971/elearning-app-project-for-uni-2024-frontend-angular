export interface Subject {
  id: number;
  name: string;
  ects: number;
  archived: boolean;
}
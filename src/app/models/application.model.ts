export interface Application {
  id: number;
  applicationDate: string;
  passed: boolean;
  grade: number;
  archived: boolean;
  studentId: number;
  examId: number;
}
export interface PaymentWithNames {
  id: number;
  bankAccount: string;
  amount: number;
  dateTime: string;
  student: string;
	archived: false;
}
export interface Student {
  id: number;
  name: string;
  email: string;
  dateOfBirth: string;
  accountBalance: number;
}
export interface ApplicationWithNames {
  id: number;
  applicationDate: string;
  passed: boolean;
  grade: number;
  archived: boolean;
  student: string;
  exam: string;
}
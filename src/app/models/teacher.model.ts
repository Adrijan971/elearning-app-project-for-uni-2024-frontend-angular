export interface Teacher {
  id: number;
  name: string;
  email: string;
  dateOfBirth: string;
  teacherType : string;
}
export interface ExamWithNames {
  id: number;
  name: string;
  teacherName: string;
  subjectName: string;
  dateTime: string;
  archived: boolean;
}
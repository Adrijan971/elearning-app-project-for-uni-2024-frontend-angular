export interface Administrator {
  id: number;
  name: string;
  email: string;
  dateOfBirth: string;
}
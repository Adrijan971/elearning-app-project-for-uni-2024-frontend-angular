export interface DocumentWithNames {
  id: number;
  name: string;
  content: string;
  student: string;
  archived: boolean;
}
export interface Payment {
  id: number;
  bankAccount: string;
  amount: number;
  dateTime: string;
  studentId: number;
	archived: false;
}
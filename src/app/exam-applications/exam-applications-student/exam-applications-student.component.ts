import { Component, OnInit } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { RouterModule, Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ExamService } from '../../services/exam.service';
import { ExamWithNames } from '../../models/examWithNames';
import { ExamUpdateDTO } from '../../DTOs/updateDTOs/examUpdateDTO.model';
import { ApplicationWithNames } from '../../models/applicationWithNames.model';
import { ApplicationService } from '../../services/application.service';

@Component({
  selector: 'app-exam-application-list',
  standalone: true,
  imports: [
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    RouterModule,
    CommonModule,
    MatToolbarModule
  ],
  templateUrl: './exam-applications-student.component.html',
  styleUrl: './exam-applications-student.component.css'
})
export class ExamApplicationsStudentComponent implements OnInit{

  applications: ApplicationWithNames[] = [];
  displayedColumns: string[] = ['applicationDate', 'student', 'exam', 'passed', 'grade'];

  constructor(
    private applciationService: ApplicationService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loadApplications();
  }

  loadApplications(): void {
    this.applciationService.getApplicationsForStudent(Number(localStorage.getItem("userId"))).subscribe(
      (data: ApplicationWithNames[]) => {
        this.applications = data;
        
        this.applications.forEach((application) => {
          application.applicationDate = application.applicationDate.split("T")[0];
        }); 
      },
      (error) => {
        console.error('Failed to load applications', error);
      }
    );

  }

  editApplication(id: number): void {
    // this.router.navigate([`/exam/edit/${id}`]);
  }

  deleteApplication(id: number): void {
    // this.examService.deleteExam(id).subscribe(
    //   () => {
    //     console.log('Deleted subject with ID:', id);
    //     this.loadExams(); // Reload exams after deletion
    //   },
    //   (error) => {
    //     console.error('Failed to delete subject', error);
    //   }
    // );
  }

}

import { Component, OnInit } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { RouterModule, Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ExamService } from '../../services/exam.service';
import { ExamWithNames } from '../../models/examWithNames';
import { ExamUpdateDTO } from '../../DTOs/updateDTOs/examUpdateDTO.model';
import { ApplicationWithNames } from '../../models/applicationWithNames.model';
import { ApplicationService } from '../../services/application.service';
import { GradeInputDialogComponent } from '../../dialogs/grade-input-dialog/grade-input-dialog.component';

import { MatDialog, MatDialogModule } from '@angular/material/dialog';

@Component({
  selector: 'app-exam-application-list',
  standalone: true,
  imports: [
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    RouterModule,
    CommonModule,
    MatToolbarModule,
    // MatDialog,
    MatDialogModule
  ],
  templateUrl: './exam-applications-teacher.component.html',
  styleUrl: './exam-applications-teacher.component.css'
})
export class ExamApplicationsTeacherComponent implements OnInit{

  applications: ApplicationWithNames[] = [];
  displayedColumns: string[] = ['applicationDate', 'student', 'exam', 'archived', 'passed', 'grade', 'actions'];

  teacherRole: string = '';
  
  constructor(
    public dialog: MatDialog,  // Public is here to be accessed in template also.
    private applciationService: ApplicationService,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.loadApplications();
    this.teacherRole = localStorage.getItem("teacherRole") || '';
  }

  isProfessor(): boolean{
    if(this.teacherRole==="PROFESSOR"){
      return true;
    }else{
      return false;
    }
  }

  loadApplications(): void {
    this.applciationService.getApplicationsForTeacher(Number(localStorage.getItem("userId"))).subscribe(
      (data: ApplicationWithNames[]) => {
        this.applications = data;

        this.applications.forEach((application) => {
          application.applicationDate = application.applicationDate.split("T")[0];
        }); 
      },
      (error) => {
        console.error('Failed to load applications', error);
      }
    );

  }

  editApplication(id: number): void {
    // this.router.navigate([`/exam/edit/${id}`]);
  }

  passApplication(id: number, grade: number): void {
    this.applciationService.passApplication(id, grade).subscribe(
      () => {
        console.log('Passed an application with ID:', id);
        this.loadApplications();
      },
      (error) => {
        console.error('Failed to pass an application', error);
      }
    );
  }

  
  openDialog(application_id: number): void {

    const dialogRef = this.dialog.open(GradeInputDialogComponent, {
      data: { number: 0 },
    });

    dialogRef.afterClosed().subscribe((grade) => {
      if (grade !== undefined) {

        this.passApplication(application_id,grade);
        
      } else {
        console.log('Dialog was closed without input');
      }
    });
  }

}

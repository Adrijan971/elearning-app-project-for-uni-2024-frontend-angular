import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
// import { MatErrorModule } from '@angular/material/form-field';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { ExamService } from '../../services/exam.service';
import { ApplicationCreateDTO } from '../../DTOs/createDTOs/applicationCreateDTO.model';
import { MatSnackBarModule, MatSnackBar } from '@angular/material/snack-bar';
import { Router, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ApplicationService } from '../../services/application.service';
import { StudentService } from '../../services/student.service';
import { Student } from '../../models/student.model';

@Component({
  selector: 'app-exam-application',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatDatepickerModule,
    MatToolbarModule,
    MatNativeDateModule,
    MatIconModule,
    MatSelectModule,
    CommonModule,
    MatSnackBarModule,
    RouterModule
  ],
  templateUrl: './exam-application.component.html',
  styleUrl: './exam-application.component.css'
})
export class ExamApplicationComponent implements OnInit {


  currentBalance: string = '';

  applyForm: FormGroup;
  submitted = false;

  exams: any[] = [];

  constructor(
    private fb: FormBuilder, 
    private applicationService: ApplicationService, 
    private examService: ExamService,
    private studentService: StudentService,
    private snackBar: MatSnackBar,
    private router: Router

  ) {
    this.applyForm = this.fb.group({
      examName: ['', Validators.required],
    });

    let student: Student;

    this.studentService.getStudent(Number(localStorage.getItem("userId"))).subscribe(
      (response: Student) => {
        student = response;
        this.currentBalance = student.accountBalance.toString();
      },
      (error) => {
        console.error('Error fetching student data', error);
      }
    );

  }

  ngOnInit(): void {
    this.studentService.getExamsForStudent(Number(localStorage.getItem("userId"))).subscribe((data: any[]) => {
      this.exams = data;
    });
  }

  onSubmit(): void {
    this.submitted = true;
  
    if (this.applyForm.valid) {
      const examId = this.applyForm.get('examName')?.value;
      const studentId = Number(window.localStorage.getItem('userId'));
  
      const now = new Date();
      const dateTime = now.toISOString().split('.')[0]; // Remove milliseconds
  
      const applicationDTO: ApplicationCreateDTO = {
        applicationDate: dateTime,
        examId: examId,
        studentId: studentId,
      };
  
      // Check if the student already has an application for the selected exam
      this.applicationService.getApplications().subscribe(
        (applications) => {
          const existingApplication = applications.find(
            (application) => application.examId === examId && application.studentId === studentId
          );
  
          if (existingApplication !== undefined && existingApplication !== null) {
            // If an application already exists for this exam, show a warning and prevent submission
            this.snackBar.open('You have already applied for this exam!', 'Close', {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
            });
            console.log('RETURN');
            return;  // Exit early, don't proceed with the application creation
          }
  
          // If no existing application, proceed with creating a new one
          this.applicationService.createApplication(applicationDTO).subscribe(
            (response) => {
              console.log('Application created successfully:', response);
  
              this.snackBar.open('Application created successfully!', 'Close', {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
              });
  
              this.router.navigate([`/exam-applications-student/list`]);
            },
            (error) => {
              console.error('Error creating Application:', error);
  
              this.snackBar.open('Not enough funds on your account!', 'Close', {
                duration: 3000,
                verticalPosition: 'top',
                horizontalPosition: 'right',
              });
            }
          );
        },
        (error) => {
          console.error('Error fetching applications:', error);
  
          this.snackBar.open('Failed to check existing applications!', 'Close', {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
          });
        }
      );
    }
  }
  

}

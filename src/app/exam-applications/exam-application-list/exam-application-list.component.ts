import { Component, OnInit } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { RouterModule, Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ExamService } from '../../services/exam.service';
import { ExamWithNames } from '../../models/examWithNames';
import { ExamUpdateDTO } from '../../DTOs/updateDTOs/examUpdateDTO.model';
import { ApplicationWithNames } from '../../models/applicationWithNames.model';
import { ApplicationService } from '../../services/application.service';

@Component({
  selector: 'app-exam-application-list',
  standalone: true,
  imports: [
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    RouterModule,
    CommonModule,
    MatToolbarModule
  ],
  templateUrl: './exam-application-list.component.html',
  styleUrl: './exam-application-list.component.css'
})
export class ExamApplicationListComponent implements OnInit{

  applications: ApplicationWithNames[] = [];
  displayedColumns: string[] = ['id', 'applicationDate', 'student', 'exam', 'archived', 'passed', 'actions'];

  constructor(
    private applciationService: ApplicationService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loadApplications();
  }

  loadApplications(): void {
    this.applciationService.getApplicationsWithNames().subscribe(
      (data: ApplicationWithNames[]) => {
        this.applications = data;

        this.applications.forEach((application) => {
          application.applicationDate = application.applicationDate.split("T")[0];
        }); 
      },
      (error) => {
        console.error('Failed to load applications', error);
      }
    );

  }

  editApplication(id: number): void {
    // this.router.navigate([`/exam/edit/${id}`]);
  }

  deleteApplication(id: number): void {
    this.applciationService.archiveApplication(id).subscribe(
      () => {
        console.log('Archived application with ID:', id);
        this.loadApplications();
      },
      (error) => {
        console.error('Failed to archive application', error);
      }
    );
  }

}

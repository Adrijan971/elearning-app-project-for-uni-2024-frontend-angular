import { Component, OnInit } from '@angular/core';
import { PdfService } from '../pdf-service.service';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { ExamService } from '../services/exam.service';
import { ExamWithNames } from '../models/examWithNames';
import { ApplicationWithNames } from '../models/applicationWithNames.model';
import { ApplicationService } from '../services/application.service';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-example',
  templateUrl: './pdf.component.html',
  styleUrls: ['./pdf.component.css'],
  standalone: true,
  imports: [CommonModule,MatButtonModule],
})

export class Pdf implements OnInit{
  constructor(
    private pdfService: PdfService,
    private route: ActivatedRoute,
    private examService: ExamService,
    private applicationService: ApplicationService
  ) {}

  examId!:number;

  examName!:String;
  examTeacher!:String;
  examSubject!:String;
  examDate!:String;
  avgGrade!:String;

  // list of applications for the exam
  applications: any[] = [];

  ngOnInit(): void {

    // Get exam ID from the route
    this.examId = +this.route.snapshot.paramMap.get('id')!;
    
    this.loadApplications(this.examId);

    this.examService.getExamWithNames(this.examId).subscribe((data: any) => {
    
      this.examName = data.name;
      this.examTeacher = data.teacherName;
      this.examSubject = data.subjectName;
      this.examDate = data.dateTime.split("T")[0];
      
    });

  }

  convertBooleanToYesNo():void{
    this.applications.forEach((application) => {

      if(application.passed==true){
        application.passed="yes";
      }else{
        application.passed="no";
      }
    });
  }

  formatDate(): void{
    this.applications.forEach((application) => {
      application.applicationDate = application.applicationDate.split("T")[0];
    });
  }

  calculateAvgGrade(): void{
    let grades: number[] = [];

    this.applications.forEach((application) => {
      grades.push(application.grade);
    });

    let averageGrade = 0;
    
    grades.forEach((grade) => {
      averageGrade = averageGrade + grade;
    });

    let formatted: string = (averageGrade/grades.length).toFixed(2);
    this.avgGrade = formatted;
  }

  loadApplications(id:number): void {
    this.applicationService.getApplicationsForExam(id).subscribe(
      (data: ApplicationWithNames[]) => {
        this.applications = data;
        this.calculateAvgGrade();
        this.formatDate();
        this.convertBooleanToYesNo();
      },
      (error) => {
        console.error('Failed to load applications', error);
      }
    );
  }

  downloadPdf() {
    this.pdfService.generatePdf('pdfContent', 'example.pdf');
  }
}
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { CommonModule } from '@angular/common';
import { MatSelectModule } from '@angular/material/select';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core'
import { ExamService } from '../../services/exam.service';
import { TeacherService } from '../../services/teacher.service';
import { Exam } from '../../models/exam.model';
import { ExamUpdateDTO } from '../../DTOs/updateDTOs/examUpdateDTO.model';
import { MatSnackBarModule, MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-exam-edit',
  standalone: true,
  imports: [
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatDividerModule,
    CommonModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule
  ],
  templateUrl: './exam-edit.component.html',
  styleUrl: './exam-edit.component.css'
})
export class ExamEditComponent implements OnInit{

  examForm!: FormGroup;
  examId!: number;

  teachers: any[] = [];

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private examService: ExamService,
    private teacherService: TeacherService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {

    this.teacherService.getTeachers().subscribe((data: any[]) => {
      this.teachers = data;
    });

    // Initialize the form
    this.examForm = this.fb.group({
      teacher: ['', Validators.required],
      dateTime: ['', Validators.required],
    });

    // Get subject ID from the route
    this.examId = +this.route.snapshot.paramMap.get('id')!;
    this.loadSubjectData(this.examId);
  }

  // Use the SubjectService to load subject data
  loadSubjectData(id: number): void {
    this.examService.getExam(id).subscribe(
      (exam: Exam) => {
        this.examForm.get('dateTime')?.setValue(exam.dateTime);
        this.examForm.get('teacher')?.setValue(exam.teacherId);
      },
      (error) => {
        console.error('Failed to load exam', error);
      }
    );
  }

  onSubmit(): void {
    if (this.examForm.valid) {

      let dateTime = this.examForm.get('dateTime')?.value;
      console.log("before conversion:",dateTime);

      // Ensure `dateTime` is a `Date` object
      if (!(dateTime instanceof Date)) {
        dateTime = new Date(dateTime);
      }

      const examDateTime = dateTime.toISOString().split('.')[0]; ; // Convert to ISO/UTC format and remove milliseconds
      console.log("after conversion!!!!!",examDateTime);

      const updatedExam: ExamUpdateDTO = {
        teacherId : this.examForm.get('teacher')?.value,
        dateTime : examDateTime
      };

      console.log("updatedExamTime",updatedExam.dateTime);
      this.examService.updateExam(this.examId, updatedExam).subscribe(
        (response) => {
          console.log('Exam updated successfully:', response);

          this.snackBar.open('Exam updated successfully!', 'Close', {
              duration: 3000,
              verticalPosition: 'top',
              horizontalPosition: 'right',
          });
          this.router.navigate(['exam/list']);
        },
        (error) => {
          console.error('Error updating Exam:', error);

          this.snackBar.open('Error, Exam was not updated!', 'Close', {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
        })}
      );
    }
  }

  onCancel(): void {
    this.router.navigate(['exam/list']);
  }


}

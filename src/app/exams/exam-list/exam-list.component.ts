import { Component, OnInit } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { RouterModule, Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ExamService } from '../../services/exam.service';
import { ExamWithNames } from '../../models/examWithNames';
import { ExamUpdateDTO } from '../../DTOs/updateDTOs/examUpdateDTO.model';
import { AuthService } from '../../auth.service';


@Component({
  selector: 'app-exam-list',
  standalone: true,
  imports: [
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    RouterModule,
    CommonModule,
    MatToolbarModule
  ],
  templateUrl: './exam-list.component.html',
  styleUrl: './exam-list.component.css'
})
export class ExamListComponent implements OnInit{

  exams: ExamWithNames[] = [];
  displayedColumns: string[] = ['id', 'name', 'teacherName', 'subjectName', 'dateTime', 'archived', 'actions'];

  userRole: string = '';

  constructor(
    private examService: ExamService,
    private router: Router,
    private authService : AuthService
  ) {
    this.userRole = this.authService.getCurrentUserRole() || "";
  }

  isAdministrator(): boolean {
    return this.userRole === 'ADMINISTRATOR';
  }

  isTeacher(): boolean {
    return this.userRole === 'TEACHER';
  }

  isStudent(): boolean {
    return this.userRole === 'STUDENT';
  }

  ngOnInit(): void {
    this.loadExams();
  }

  loadExams(): void {
    this.examService.getExamsWithNames().subscribe(
      (data: ExamWithNames[]) => {
        this.exams = data;

        this.exams.forEach((exam) => {
          exam.dateTime = exam.dateTime.split("T")[0];
        }); 
      },
      (error) => {
        console.error('Failed to load exams', error);
      }
    );

  }

  editExam(id: number): void {
    this.router.navigate([`/exam/edit/${id}`]);
  }

  deleteExam(id: number): void {
    this.examService.deleteExam(id).subscribe(
      () => {
        console.log('Deleted subject with ID:', id);
        this.loadExams(); // Reload exams after deletion
      },
      (error) => {
        console.error('Failed to delete subject', error);
      }
    );
  }

}

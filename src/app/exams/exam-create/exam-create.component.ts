import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
// import { MatErrorModule } from '@angular/material/form-field';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { SubjectService } from '../../services/subject.service';
import { TeacherService } from '../../services/teacher.service';
import { ExamService } from '../../services/exam.service';
import { ExamCreateDTO } from '../../DTOs/createDTOs/examCreateDTO.model';
import { MatSnackBarModule, MatSnackBar } from '@angular/material/snack-bar';
import { Router, RouterModule } from '@angular/router';

import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-exam-create',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatDatepickerModule,
    MatToolbarModule,
    MatNativeDateModule,
    MatIconModule,
    MatSelectModule,
    CommonModule,
    MatSnackBarModule,
    RouterModule
  ],
  templateUrl: './exam-create.component.html',
  styleUrl: './exam-create.component.css'
})
export class ExamCreateComponent implements OnInit{

  examForm: FormGroup;
  submitted = false;

  subjects: any[] = [];
  teachers: any[] = [];

  constructor(
    private fb: FormBuilder, 
    private teacherService: TeacherService, 
    private subjectService: SubjectService,
    private examService: ExamService,
    private snackBar: MatSnackBar,
    private router: Router

  ) {
    this.examForm = this.fb.group({
      examName: ['', Validators.required],
      subject: ['', Validators.required],
      teacher: ['', Validators.required],
      examDate: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.subjectService.getSubjects().subscribe((data: any[]) => {
      this.subjects = data;
    });
    this.teacherService.getTeachers().subscribe((data: any[]) => {
      this.teachers = data;
    });
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.examForm.valid) {

      const examDate: Date = this.examForm.get('examDate')?.value || new Date(); // Get the selected date or default to now
      const examTime = examDate.toISOString().split('.')[0]; // Convert to ISO format and remove milliseconds

      const examDTO: ExamCreateDTO = {
        name: this.examForm.get('examName')?.value,
        teacherId: this.examForm.get('teacher')?.value,
        subjectId: this.examForm.get('subject')?.value,
        dateTime: examTime,
      };

      this.examService.createExam(examDTO).subscribe(
        (response) => {
          console.log('Exam created successfully:', response);

        this.snackBar.open('Exam created successfully!', 'Close', {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
        });

        this.router.navigate([`/exam/list`]);
        },
        (error) => {
          console.error('Error creating exam:', error);

          this.snackBar.open('Error, exam was not created!', 'Close', {
            duration: 3000,
            verticalPosition: 'top',
            horizontalPosition: 'right',
        });
        }
      );
    }
  }
}

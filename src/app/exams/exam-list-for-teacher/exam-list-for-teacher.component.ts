import { Component, OnInit } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { RouterModule, Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ExamService } from '../../services/exam.service';
import { ExamWithNames } from '../../models/examWithNames';
import { ExamUpdateDTO } from '../../DTOs/updateDTOs/examUpdateDTO.model';
import { PdfService } from '../../pdf-service.service';


@Component({
  selector: 'app-exam-list',
  standalone: true,
  imports: [
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    RouterModule,
    CommonModule,
    MatToolbarModule
  ],
  templateUrl: './exam-list-for-teacher.component.html',
  styleUrl: './exam-list-for-teacher.component.css'
})
export class ExamListForTeacherComponent implements OnInit{

  exams: ExamWithNames[] = [];
  displayedColumns: string[] = ['name', 'teacherName', 'subjectName', 'dateTime', 'archived', 'actions'];

  currentTeacherId: number = Number(localStorage.getItem("userId"));

  constructor(
    private examService: ExamService,
    private router: Router,
    private pdfService: PdfService
  ) {}

  ngOnInit(): void {
    this.loadExams();
  }

  loadExams(): void {
    this.examService.getExamsWithNamesForTeacher(this.currentTeacherId).subscribe(
      (data: ExamWithNames[]) => {
        this.exams = data;

        this.exams.forEach((exam) => {
          exam.dateTime = exam.dateTime.split("T")[0];
        }); 
      },
      (error) => {
        console.error('Failed to load exams', error);
      }
    );

  }

  editExam(id: number): void {
    this.router.navigate([`/exam/edit/${id}`]);
  }

  downloadPDF(id: number): void {
    this.router.navigate([`/pdf/${id}`]);
  }

  deleteExam(id: number): void {
    this.examService.deleteExam(id).subscribe(
      () => {
        console.log('Deleted subject with ID:', id);
        this.loadExams(); // Reload exams after deletion
      },
      (error) => {
        console.error('Failed to delete subject', error);
      }
    );
  }

}

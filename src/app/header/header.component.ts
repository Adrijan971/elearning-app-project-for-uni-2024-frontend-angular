import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatButtonModule } from '@angular/material/button';
import {MatIconModule } from '@angular/material/icon';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [MatButtonModule, MatIconModule, CommonModule],
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})
export class HeaderComponent implements OnInit{
  @Input() pageTitle!: string;
  @Input() logoSrc!: string;

  constructor(private authService : AuthService, private router: Router){}

  isAuthenticated: boolean = false;

  username: string = '';
  firstName: string = '';
  lastName: string = '';
  role: string = '';
  teacherRole: string = '';


  ngOnInit(): void {
    this.username = localStorage.getItem("username") || '';
    this.firstName = localStorage.getItem("name") || '';
    this.lastName = localStorage.getItem("lastname") || '';
    this.role = localStorage.getItem("userRole") || '';
    this.teacherRole = localStorage.getItem("teacherRole") || '';

    // Subscribe to authentication changes
    this.authService.isAuthenticated$.subscribe((authStatus: boolean) => {
      this.isAuthenticated = authStatus;
    });
  }

  @Output() clicked: EventEmitter<void> = new EventEmitter<void>();

  onClickBurgerButton() {
    this.clicked.emit();
  }

  logout() {
    this.authService.logout();
    // this.router.navigate(['/login']); // moze i ovako preko rute
    window.location.href = '/login';  // refresh the page
  }
}
